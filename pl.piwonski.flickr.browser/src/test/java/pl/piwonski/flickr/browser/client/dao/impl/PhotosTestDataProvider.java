package pl.piwonski.flickr.browser.client.dao.impl;

import java.util.ArrayList;

import org.testng.annotations.DataProvider;

import pl.piwonski.flickr.browser.client.entity.FlickrPhoto;
import pl.piwonski.flickr.browser.client.entity.FlickrPhotos;
import pl.piwonski.flickr.browser.domain.Photo;
import pl.piwonski.flickr.browser.domain.Photos;

/**
 * @author krzysztof@piwonski.pl
 */
public class PhotosTestDataProvider {
	private static final Integer PAGE_NO = 1;
	private static final Long ALL_RESULTS_COUNT = 100L;

	private static final Long PHOTO_1_ID = 1L;
	private static final String PHOTO_1_TITLE = "Title 1";
	private static final String PHOTO_1_FARM_ID = "farm1";
	private static final String PHOTO_1_SERVER_ID = "server1";
	private static final String PHOTO_1_SECRET = "secret1";
	private static final String PHOTO_1_SMALL_URL = "http://farm1/server1/1/secret1/$SIZE$.jpg";
	private static final String PHOTO_1_BIG_URL = "http://farm1/server1/1/secret1/$SIZE$.jpg";

	private static final Long PHOTO_2_ID = 2L;
	private static final String PHOTO_2_TITLE = "Title 2";
	private static final String PHOTO_2_FARM_ID = "farm2";
	private static final String PHOTO_2_SERVER_ID = "server2";
	private static final String PHOTO_2_SECRET = "secret2";
	private static final String PHOTO_2_SMALL_URL = "http://farm2/server2/2/secret2/$SIZE$.jpg";
	private static final String PHOTO_2_BIG_URL = "http://farm2/server2/2/secret2/$SIZE$.jpg";

	@DataProvider(name = "getTestData")
	public static Object[][] getTestData() {
		return new Object[][] { new Object[] { buildFlickrPhotos(), buildPhotos() } };
	}

	private static Photos buildPhotos() {
		return new Photos(getPhotosList(), PAGE_NO, ALL_RESULTS_COUNT);
	}

	private static ArrayList<Photo> getPhotosList() {
		ArrayList<Photo> photos = new ArrayList<Photo>();
		photos.add(new Photo(PHOTO_1_TITLE, PHOTO_1_SMALL_URL, PHOTO_1_BIG_URL));
		photos.add(new Photo(PHOTO_2_TITLE, PHOTO_2_SMALL_URL, PHOTO_2_BIG_URL));
		return photos;
	}

	private static FlickrPhotos buildFlickrPhotos() {
		final FlickrPhotos flickrPhotos = new FlickrPhotos();
		flickrPhotos.allResultsCount = ALL_RESULTS_COUNT;
		flickrPhotos.pageNo = PAGE_NO;
		flickrPhotos.photos = getFlickrPhotosList();
		return flickrPhotos;
	}

	private static ArrayList<FlickrPhoto> getFlickrPhotosList() {
		ArrayList<FlickrPhoto> photos = new ArrayList<FlickrPhoto>();
		photos.add(getPhoto1());
		photos.add(getPhoto2());
		return photos;
	}

	private static FlickrPhoto getPhoto1() {
		final FlickrPhoto photo = new FlickrPhoto();
		photo.title = PHOTO_1_TITLE;
		photo.farmId = PHOTO_1_FARM_ID;
		photo.serverId = PHOTO_1_SERVER_ID;
		photo.id = PHOTO_1_ID;
		photo.secret = PHOTO_1_SECRET;
		return photo;
	}

	private static FlickrPhoto getPhoto2() {
		final FlickrPhoto photo = new FlickrPhoto();
		photo.title = PHOTO_2_TITLE;
		photo.farmId = PHOTO_2_FARM_ID;
		photo.serverId = PHOTO_2_SERVER_ID;
		photo.id = PHOTO_2_ID;
		photo.secret = PHOTO_2_SECRET;
		return photo;
	}
}
