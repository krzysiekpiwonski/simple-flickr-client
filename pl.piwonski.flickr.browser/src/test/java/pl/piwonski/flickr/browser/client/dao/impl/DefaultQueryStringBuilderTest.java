package pl.piwonski.flickr.browser.client.dao.impl;

import static org.fest.assertions.Assertions.assertThat;

import org.testng.annotations.Test;

import pl.piwonski.flickr.browser.client.dao.QueryStringBuilder;

/**
 * @author krzysztof@piwonski.pl
 */
public class DefaultQueryStringBuilderTest {
	@Test(groups = "unit")
	public void shouldBuildQueryString() {
		// given
		final String pattern = "?api_key=$API_KEY$&method=xxx&format=rest&per_page=$RESULTS_PER_PAGE$"
				+ "&page=$PAGE_NO$&text=$SEARCH_PHRASE$";
		final String apiKey = "123";
		final Integer resultsPerPageCount = 10;
		final Integer pageNo = 1;
		final String searchPhrase = "test";
		final String expected = "?api_key=" + apiKey + "&method=xxx&format=rest&per_page=" + resultsPerPageCount
				+ "&page=" + pageNo + "&text=" + searchPhrase;

		// when
		final QueryStringBuilder queryStringBuilder = new DefaultQueryStringBuilder(pattern, apiKey,
				resultsPerPageCount);
		final String result = queryStringBuilder.build(pageNo, searchPhrase);

		// then
		assertThat(result).isNotNull().isNotEmpty().isEqualTo(expected);
	}
}
