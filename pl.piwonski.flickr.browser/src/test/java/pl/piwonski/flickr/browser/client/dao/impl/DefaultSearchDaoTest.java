package pl.piwonski.flickr.browser.client.dao.impl;

import static org.fest.assertions.Assertions.assertThat;

import org.mockito.Mockito;
import org.testng.annotations.Test;

import pl.piwonski.flickr.browser.client.dao.ApiClient;
import pl.piwonski.flickr.browser.client.dao.Converter;
import pl.piwonski.flickr.browser.client.dao.SearchDao;
import pl.piwonski.flickr.browser.client.entity.FlickrPhotos;
import pl.piwonski.flickr.browser.client.entity.FlickrResponse;
import pl.piwonski.flickr.browser.domain.Photos;

/**
 * @author krzysztof@piwonski.pl
 */
public class DefaultSearchDaoTest {
	@Test(groups = "unit", dataProvider = "getTestData", dataProviderClass = PhotosTestDataProvider.class)
	public void shouldReturnPhotos(final FlickrPhotos flickrPhotos, final Photos expected) {
		// given
		final Converter<FlickrPhotos, Photos> converter = Mockito.mock(Converter.class);
		Mockito.when(converter.convert(flickrPhotos)).thenReturn(expected);
		final ApiClient<FlickrResponse> apiClient = Mockito.mock(ApiClient.class);
		Mockito.when(apiClient.get(Mockito.anyString(), Mockito.anyInt())).thenReturn(getFlickrResponse(flickrPhotos));

		// when
		final SearchDao<Photos> searchDao = new DefaultSearchDao(apiClient, converter);
		final Photos result = searchDao.search(null, null);

		// then
		assertThat(result).isNotNull().isEqualTo(expected);
	}

	private FlickrResponse getFlickrResponse(final FlickrPhotos flickrPhotos) {
		final FlickrResponse flickrResponse = new FlickrResponse();
		flickrResponse.photos = flickrPhotos;
		return flickrResponse;
	}
}
