package pl.piwonski.flickr.browser.client.dao.impl;

import static org.fest.assertions.Assertions.assertThat;

import org.mockito.Mockito;
import org.testng.annotations.Test;

import pl.piwonski.flickr.browser.client.dao.QueryStringBuilder;
import pl.piwonski.flickr.browser.client.entity.FlickrResponse;

/**
 * @author krzysztof@piwonski.pl
 */
public class FlickrApiClientTest {
	@Test(groups = "integration")
	public void shouldConnectToFlickrAndReturnResults() {
		// given
		final Integer pageNo = 1;
		final String searchPhrase = "test";
		final String queryString = "?method=flickr.photos.search&format=rest"
				+ "&api_key=8c04f0880e5b8c6618253c2498fd82b7&per_page=10&page=1&text=test";
		final QueryStringBuilder queryStringBuilder = Mockito.mock(QueryStringBuilder.class);
		Mockito.when(queryStringBuilder.build(pageNo, searchPhrase)).thenReturn(queryString);
		final String serviceUrl = "http://api.flickr.com/services/rest/";

		// when
		final FlickrApiClient apiClient = new FlickrApiClient(queryStringBuilder, serviceUrl);
		final FlickrResponse result = apiClient.get(searchPhrase, pageNo);

		// then
		assertThat(result).isNotNull();
		System.out.println(result);
	}
}
