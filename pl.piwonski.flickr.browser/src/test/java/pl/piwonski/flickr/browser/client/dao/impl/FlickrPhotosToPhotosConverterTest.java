package pl.piwonski.flickr.browser.client.dao.impl;

import static org.fest.assertions.Assertions.assertThat;

import org.testng.annotations.Test;

import pl.piwonski.flickr.browser.client.dao.Converter;
import pl.piwonski.flickr.browser.client.entity.FlickrPhotos;
import pl.piwonski.flickr.browser.domain.Photos;

/**
 * @author krzysztof@piwonski.pl
 */
public class FlickrPhotosToPhotosConverterTest {
	@Test(groups = "unit", dataProvider = "getTestData", dataProviderClass = PhotosTestDataProvider.class)
	public void shouldConvertObject(final FlickrPhotos objectToConvert, final Photos expected) {
		// given
		final String photoUrlPattern = "http://$FARM_ID$/$SERVER_ID$/$PHOTO_ID$/$SECRET$/$SIZE$.jpg";
		final String photoSizeSmall = "s";
		final String photoSizeBig = "b";

		// when
		final Converter<FlickrPhotos, Photos> converter = new FlickrPhotosToPhotosConverter(photoUrlPattern,
				photoSizeSmall, photoSizeBig);
		final Photos result = converter.convert(objectToConvert);

		// then
		assertThat(result).isNotNull().isEqualTo(expected);
	}
}
