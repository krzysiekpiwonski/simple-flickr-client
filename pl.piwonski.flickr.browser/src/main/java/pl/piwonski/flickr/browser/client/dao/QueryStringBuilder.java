package pl.piwonski.flickr.browser.client.dao;

/**
 * @author krzysztof@piwonski.pl
 */
public interface QueryStringBuilder {
    String build(Integer pageNo, String searchPhrase);
}
