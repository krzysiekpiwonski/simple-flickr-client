package pl.piwonski.flickr.browser.ui.parts;

import javax.inject.Inject;

import org.eclipse.e4.ui.services.IStylingEngine;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;

/**
 * @author krzysztof@piwonski.pl
 */
public final class PhotosPart {
	@Inject
	private IStylingEngine engine;
	@Inject
	private Composite parent;

	public void createHeader() {
		final GridData layoutData = new GridData();
		layoutData.horizontalAlignment = SWT.FILL;
		layoutData.horizontalSpan = 3;

		final Label label = new Label(parent, SWT.CENTER);
		label.setText("Photos");
		label.setLayoutData(layoutData);

		//engine.setId(label, "PhotosPartLabelHeader");
	}
}
