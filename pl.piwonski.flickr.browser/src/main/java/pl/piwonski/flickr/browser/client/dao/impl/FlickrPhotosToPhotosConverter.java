package pl.piwonski.flickr.browser.client.dao.impl;

import java.util.ArrayList;
import java.util.List;

import pl.piwonski.flickr.browser.client.dao.Converter;
import pl.piwonski.flickr.browser.client.entity.FlickrPhoto;
import pl.piwonski.flickr.browser.client.entity.FlickrPhotos;
import pl.piwonski.flickr.browser.domain.Photo;
import pl.piwonski.flickr.browser.domain.Photos;

/**
 * @author krzysztof@piwonski.pl
 */
public final class FlickrPhotosToPhotosConverter implements Converter<FlickrPhotos, Photos> {
	private final String photoUrlPattern;
	private final String photoSizeSmall;
	private final String photoSizeBig;

	public FlickrPhotosToPhotosConverter(final String photoUrlPattern, final String photoSizeSmall,
			final String photoSizeBig) {
		this.photoUrlPattern = photoUrlPattern;
		this.photoSizeSmall = photoSizeSmall;
		this.photoSizeBig = photoSizeBig;
	}

	@Override
	public Photos convert(final FlickrPhotos objectToConvert) {
		System.out.println(objectToConvert);
		return new Photos(getPhotos(objectToConvert.photos), objectToConvert.pageNo, objectToConvert.allResultsCount);
	}

	private List<Photo> getPhotos(final List<FlickrPhoto> flickrPhotos) {
		final List<Photo> photos = new ArrayList<Photo>(flickrPhotos.size());
		for (final FlickrPhoto flickrPhoto : flickrPhotos) {
			photos.add(new Photo(flickrPhoto.title, getPhotoUrl(flickrPhoto, photoSizeSmall), getPhotoUrl(flickrPhoto,
					photoSizeBig)));
		}
		return photos;
	}

	private String getPhotoUrl(final FlickrPhoto flickrPhoto, final String size) {
		String url = photoUrlPattern;
		url = url.replace("$FARM_ID$", flickrPhoto.farmId);
		url = url.replace("$SERVER_ID$", flickrPhoto.serverId);
		url = url.replace("$PHOTO_ID$", "" + flickrPhoto.id);
		url = url.replace("$SECRET$", flickrPhoto.secret);
		url = url.replace("$SIZE$", size);
		return url;
	}
}
