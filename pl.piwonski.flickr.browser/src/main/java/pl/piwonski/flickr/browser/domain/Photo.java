package pl.piwonski.flickr.browser.domain;

/**
 * @author krzysztof@piwonski.pl
 */
public final class Photo {
    public final String title;
    public final String smallUrl;
    public final String bigUrl;

    public Photo(final String title, final String smallUrl, final String bigUrl) {
        this.title = title;
        this.smallUrl = smallUrl;
        this.bigUrl = bigUrl;
    }
}
