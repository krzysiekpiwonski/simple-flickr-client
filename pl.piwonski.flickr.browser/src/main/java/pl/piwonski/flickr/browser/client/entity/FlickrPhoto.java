package pl.piwonski.flickr.browser.client.entity;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author krzysztof@piwonski.pl
 */
@XmlRootElement
public class FlickrPhoto {
    @XmlAttribute(name = "id", required = true)
    public Long id;

    @XmlAttribute(name = "title", required = true)
    public String title;

    @XmlAttribute(name = "farm", required = true)
    public String farmId;

    @XmlAttribute(name = "server", required = true)
    public String serverId;

    @XmlAttribute(name = "secret", required = true)
    public String secret;

    @Override
    public String toString() {
        return "PHOTO: id=" + id + ", title='" + title + "'";
    }
}
