package pl.piwonski.flickr.browser;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;

public class Activator implements BundleActivator {
	@Override
	public void start(BundleContext context) throws Exception {
		System.out.println("Starting pl.piwonski.flickr.browser...");
	}

	@Override
	public void stop(BundleContext context) throws Exception {
		System.out.println("Stopping pl.piwonski.flickr.browser...");
	}
}
