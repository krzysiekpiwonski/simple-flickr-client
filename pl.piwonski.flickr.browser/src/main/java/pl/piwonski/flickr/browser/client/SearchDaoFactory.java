package pl.piwonski.flickr.browser.client;

import org.eclipse.e4.core.di.annotations.Creatable;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import pl.piwonski.flickr.browser.client.dao.SearchDao;
import pl.piwonski.flickr.browser.domain.Photos;

/**
 * @author krzysztof@piwonski.pl
 */
@Creatable
public final class SearchDaoFactory {
	private static final String configFilePath = "beans.xml";
	private static final ApplicationContext context = new ClassPathXmlApplicationContext(configFilePath);

	@SuppressWarnings("unchecked")
	public SearchDao<Photos> getSearchDao() {
		return context.getBean(SearchDao.class);
	}
}
