package pl.piwonski.flickr.browser.domain;

import java.util.Collections;
import java.util.List;

/**
 * @author krzysztof@piwonski.pl
 */
public final class Photos {
	private final List<Photo> photos;
	public final Integer pageNo;
	public final Long resultsCount;

	public Photos(final List<Photo> photos, final Integer pageNo, final Long resultsCount) {
		this.photos = photos;
		this.pageNo = pageNo;
		this.resultsCount = resultsCount;
	}

	public List<Photo> getPhotos() {
		return Collections.unmodifiableList(photos);
	}

}
