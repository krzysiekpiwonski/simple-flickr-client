package pl.piwonski.flickr.browser.client.dao;

/**
 * @author krzysztof@piwonski.pl
 */
public interface Converter<T, U> {
    U convert(T objectToConvert);
}
