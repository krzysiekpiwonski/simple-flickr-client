package pl.piwonski.flickr.browser.ui.handlers;

import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.ui.workbench.IWorkbench;

/**
 * @author krzysztof@piwonski.pl
 */
public class ExitHandler {
    @Execute
    public void execute(final IWorkbench workbench) {
        workbench.close();
    }
}
