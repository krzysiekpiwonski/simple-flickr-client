package pl.piwonski.flickr.browser.ui.listeners;

import javax.inject.Inject;

import org.eclipse.e4.core.di.annotations.Creatable;
import org.eclipse.e4.ui.model.application.MApplication;
import org.eclipse.e4.ui.model.application.ui.advanced.MPerspective;
import org.eclipse.e4.ui.model.application.ui.basic.MBasicFactory;
import org.eclipse.e4.ui.model.application.ui.basic.MPart;
import org.eclipse.e4.ui.model.application.ui.basic.MWindow;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;

import pl.piwonski.flickr.browser.client.SearchDaoFactory;
import pl.piwonski.flickr.browser.client.dao.SearchDao;
import pl.piwonski.flickr.browser.domain.Photos;
import pl.piwonski.flickr.browser.ui.parts.PhotosPart;

/**
 * @author krzysztof@piwonski.pl
 */
@Creatable
public class SearchListener implements Listener {
	@Inject
	private MPerspective parent;
	@Inject
	private MPart thisElement;
	@Inject
	private SearchDaoFactory daoFactory;

	@Override
	public void handleEvent(final Event event) {
		final SearchDao<Photos> searchDao = daoFactory.getSearchDao();
		final Photos results = searchDao.search("test", 1);
		final MWindow window = MBasicFactory.INSTANCE.createTrimmedWindow();
		window.setWidth(200);
		window.setHeight(300);
		window.setLabel("Title: " + results.resultsCount);
		
		System.out.println(parent.getChildren().get(0).getElementId());
		System.out.println(thisElement.getElementId());
		
		//application.getChildren().add(window);
	}
}
