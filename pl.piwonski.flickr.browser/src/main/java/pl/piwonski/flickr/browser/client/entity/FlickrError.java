package pl.piwonski.flickr.browser.client.entity;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author krzysztof@piwonski.pl
 */
@XmlRootElement
public class FlickrError {
    @XmlAttribute(name = "code", required = true)
    public Integer code;

    @XmlAttribute(name = "msg", required = true)
    public String message;

    @Override
    public String toString() {
        return "ERROR: code=" + code + ", message='" + message + "'";
    }
}
