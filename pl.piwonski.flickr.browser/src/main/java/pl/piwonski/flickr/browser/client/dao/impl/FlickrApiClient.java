package pl.piwonski.flickr.browser.client.dao.impl;

import javax.ws.rs.core.MediaType;

import pl.piwonski.flickr.browser.client.dao.ApiClient;
import pl.piwonski.flickr.browser.client.dao.QueryStringBuilder;
import pl.piwonski.flickr.browser.client.entity.FlickrResponse;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

/**
 * @author krzysztof@piwonski.pl
 */
public final class FlickrApiClient implements ApiClient<FlickrResponse> {
	private final QueryStringBuilder queryStringBuilder;
	private final String serviceUrl;

	public FlickrApiClient(final QueryStringBuilder queryStringBuilder, final String serviceUrl) {
		this.queryStringBuilder = queryStringBuilder;
		this.serviceUrl = serviceUrl;
	}

	@Override
	public FlickrResponse get(final String searchPhrase, final Integer pageNo) {
		final Client client = Client.create();
		final WebResource resource = client.resource(getResourceUrl(searchPhrase, pageNo));
		final ClientResponse response = resource.accept(MediaType.APPLICATION_XML, MediaType.TEXT_XML).get(
				ClientResponse.class);
		return response.getEntity(FlickrResponse.class);
	}

	private String getResourceUrl(final String searchPhrase, final Integer pageNo) {
		return serviceUrl + queryStringBuilder.build(pageNo, searchPhrase);
	}
}
