package pl.piwonski.flickr.browser.client.entity;

/**
 * @author krzysztof@piwonski.pl
 */
public enum Status {
    ok, fail
}
