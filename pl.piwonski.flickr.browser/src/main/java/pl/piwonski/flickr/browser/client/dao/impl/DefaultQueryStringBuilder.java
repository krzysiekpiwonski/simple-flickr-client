package pl.piwonski.flickr.browser.client.dao.impl;

import pl.piwonski.flickr.browser.client.dao.QueryStringBuilder;

/**
 * @author krzysztof@piwonski.pl
 */
public final class DefaultQueryStringBuilder implements QueryStringBuilder {
	private final String queryStringPattern;
	private final String apiKey;
	private final Integer resultsPerPageCount;

	public DefaultQueryStringBuilder(final String queryStringPattern, final String apiKey,
			final Integer resultsPerPageCount) {
		this.queryStringPattern = queryStringPattern;
		this.apiKey = apiKey;
		this.resultsPerPageCount = resultsPerPageCount;
	}

	@Override
	public String build(final Integer pageNo, final String searchPhrase) {
		String queryString = queryStringPattern;
		queryString = queryString.replace("$API_KEY$", apiKey);
		queryString = queryString.replace("$RESULTS_PER_PAGE$", "" + resultsPerPageCount);
		queryString = queryString.replace("$PAGE_NO$", "" + pageNo);
		queryString = queryString.replace("$SEARCH_PHRASE$", searchPhrase);
		return queryString;
	}
}
