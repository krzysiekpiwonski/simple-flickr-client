package pl.piwonski.flickr.browser.client.dao.impl;

import pl.piwonski.flickr.browser.client.dao.ApiClient;
import pl.piwonski.flickr.browser.client.dao.Converter;
import pl.piwonski.flickr.browser.client.dao.SearchDao;
import pl.piwonski.flickr.browser.client.entity.FlickrPhotos;
import pl.piwonski.flickr.browser.client.entity.FlickrResponse;
import pl.piwonski.flickr.browser.domain.Photos;

/**
 * @author krzysztof@piwonski.pl
 */
public final class DefaultSearchDao implements SearchDao<Photos> {
	private final Converter<FlickrPhotos, Photos> converter;
	private final ApiClient<FlickrResponse> apiClient;

	public DefaultSearchDao(final ApiClient<FlickrResponse> apiClient, final Converter<FlickrPhotos, Photos> converter) {
		this.apiClient = apiClient;
		this.converter = converter;
	}

	@Override
	public Photos search(final String searchPhrase, final Integer pageNo) {
		final FlickrResponse flickrResponse = apiClient.get(searchPhrase, pageNo);
		return converter.convert(flickrResponse.photos);
	}
}
