package pl.piwonski.flickr.browser.client.dao;

/**
 * @author krzysztof@piwonski.pl
 */
public interface ApiClient<T> {
    T get(String searchPhrase, Integer pageNo);
}
