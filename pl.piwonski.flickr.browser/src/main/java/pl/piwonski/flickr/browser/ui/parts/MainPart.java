package pl.piwonski.flickr.browser.ui.parts;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.eclipse.e4.ui.services.IStylingEngine;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

import pl.piwonski.flickr.browser.ui.listeners.SearchListener;

/**
 * @author krzysztof@piwonski.pl
 */
public final class MainPart {
	@Inject
	private IStylingEngine engine;
	@Inject
	private SearchListener searchListener;

	@PostConstruct
	public void showLabel(final Composite parent) {
		parent.setLayout(new GridLayout(3, false));
		createHeader(parent);
		createSearchBox(parent);
	}

	private void createHeader(final Composite parent) {
		final GridData layoutData = new GridData();
		layoutData.horizontalAlignment = SWT.FILL;
		layoutData.horizontalSpan = 3;

		final Label label = new Label(parent, SWT.CENTER);
		label.setText("Simple Flickr Browser");
		label.setLayoutData(layoutData);

		engine.setId(label, "MainPartLabelHeader");
	}

	private void createSearchBox(Composite parent) {
		final GridData layoutData = new GridData();
		layoutData.horizontalAlignment = SWT.FILL;
		layoutData.grabExcessHorizontalSpace = true;

		final Label label = new Label(parent, SWT.CENTER);
		label.setText("Search for photos (only public):");

		final Text text = new Text(parent, SWT.BORDER);
		text.setLayoutData(layoutData);

		final Button button = new Button(parent, SWT.CENTER);
		button.setText("Search");
		button.addListener(SWT.Selection, searchListener);
	}
}
