package pl.piwonski.flickr.browser.client.entity;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author krzysztof@piwonski.pl
 */
@XmlRootElement(name = "rsp")
public class FlickrResponse {
    @XmlAttribute(name = "stat", required = true)
    public Status status;

    @XmlElement(name = "photos", required = false)
    public FlickrPhotos photos;

    @XmlElement(name = "err", required = false)
    public FlickrError error;

    @Override
    public String toString() {
        return (status == Status.ok) ? ("status=" + status + '\n' + photos) : ("status: " + status + '\n' + error);
    }
}
