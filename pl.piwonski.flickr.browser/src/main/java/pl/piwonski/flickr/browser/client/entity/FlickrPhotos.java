package pl.piwonski.flickr.browser.client.entity;

import java.util.List;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author krzysztof@piwonski.pl
 */
@XmlRootElement
public class FlickrPhotos {
    @XmlElement(name = "photo", required = true)
    public List<FlickrPhoto> photos;

    @XmlAttribute(name = "page", required = true)
    public Integer pageNo;

    @XmlAttribute(name = "perpage", required = true)
    public Integer resultsPerPageCount;

    @XmlAttribute(name = "pages", required = true)
    public Long pages;

    @XmlAttribute(name = "total", required = true)
    public Long allResultsCount;

    @Override
    public String toString() {
        return "PHOTOS: pageNo=" + pageNo + ", resultsPerPageCount=" + resultsPerPageCount + ", pages=" + pages
                + ", allResultsCount=" + allResultsCount + "\nPHOTOS LIST:" + photos;
    }
}
