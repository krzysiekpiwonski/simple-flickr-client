package pl.piwonski.flickr.browser.client.dao;

/**
 * @author krzysztof@piwonski.pl
 */
public interface SearchDao<T> {
    T search(String searchPhrase, Integer pageNo);
}
